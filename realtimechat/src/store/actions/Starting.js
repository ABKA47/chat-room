import * as actionTypes from './actionTypes';

export const getInputs = (startingObject) => {
    return {
        type: actionTypes.GETINPUTS,
        startingObject: startingObject
    }
}