import * as actionTypes from '../actions/actionTypes'
import { updatedObject } from '../Utility';

const initialState = {
    starting: {
        name: {
            type: 'input',
            elementConfig: {
                type: 'text'
            },
            value: '',
            placeHolder: 'Type your Name ...'
        },
        chatRoom: {
            type: 'select',
            elementConfig: {
                options: [
                    { value: 'CS', displayValue: 'CS' },
                    { value: 'NOOBLARGIREMEZ', displayValue: 'NOOBLARGİREMEZ' },
                    { value: 'ASS', displayValue: 'ASS' },

                ],
            },
            value: 'CS'
        },
        users: []
    }
}

const getInputs = (state, action) => {
    return updatedObject(state, { starting: action.startingObject })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GETINPUTS:
            return getInputs(state, action)
        default:
            return state;
    }
}

export default reducer;