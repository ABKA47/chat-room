import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as actions from '../store/actions/index'

import { Route, Link } from 'react-router-dom'
import Input from '../components/UI/Input'

class Starting extends Component {

    inputChangeHandler = (event, inputIdentifier) => {
        const updatedState = { ...this.props.starting }
        const updatedElement = { ...updatedState[inputIdentifier] }
        updatedElement.value = event.target.value
        updatedState[inputIdentifier] = updatedElement
        this.props.onGetInputs(updatedState)
    }
    render() {
        const inputArray = [];
        for (let key in this.props.starting) {
            inputArray.push({
                id: key,
                config: this.props.starting[key]
            })
        }
        return (
            <div className="container card mt-3">
                <form>
                    <div className="mb-3 mt-3">
                        {inputArray.map(element => (
                            <Input
                                key={element.id}
                                elementType={element.config.type}
                                elementConfig={element.config.elementConfig}
                                value={element.config.value}
                                placeHolder={element.config.placeHolder}
                                changed={(event) => this.inputChangeHandler(event, element.id)}
                            />
                        )

                        )}
                    </div>
                    <Link to="/home"><button type="submit" className="btn btn-primary" >JOIN CHAT</button></Link>

                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    starting: state.starting.starting
})

const mapDispatchToProps = (dispatch) => ({
    onGetInputs: (updatedStarting) => dispatch(actions.getInputs(updatedStarting))
})


export default connect(mapStateToProps, mapDispatchToProps)(Starting);