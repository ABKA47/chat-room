import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Home from './components/Home'
import Starting from './containers/Starting'


function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/" exact component={Starting} />
        <Route path="/home" component={Home} />
      </Switch>

    </div>
  );
}

export default App;
