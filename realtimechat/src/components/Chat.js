import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { ApolloClient, InMemoryCache, ApolloProvider, gql, useSubscription, useMutation } from '@apollo/client';
import { WebSocketLink } from '@apollo/client/link/ws';

const link = new WebSocketLink({
    uri: 'ws://localhost:4000/',
    options: {
        reconnect: true
    }
});
const client = new ApolloClient({
    link,
    uri: 'http://localhost:4000/',
    cache: new InMemoryCache()
});

const GET_MESSAGES = gql`
subscription{
    messages{
        id
        user
        content
        chatRoom
    }
} `

const POST_MESSAGES = gql`
mutation($user: String! , $content: String! , $room: String!){
    postMessage(user: $user , content: $content , chatRoom : $room )
  }
`


const Messages = ({ user, room }) => {
    const { data } = useSubscription(GET_MESSAGES);
    console.log("chatRoom", data)
    console.log("room", room)
    if (!data) {
        return null;
    }
    return (
        <>

            {data.messages.map(({ id, user: messageUser, content, chatRoom }) =>
                chatRoom === room ?
                    (
                        <div
                            key={id}
                            style={{
                                display: 'flex',
                                justifyContent: user === messageUser ? "flex-end" : "flex-start",
                                paddingBottom: "1em"
                            }}
                        >
                            {user !== messageUser && (
                                <div
                                    style={{
                                        height: 50,
                                        width: 50,
                                        marginRight: "0.5em",
                                        border: "2px solid #e5e6ea",
                                        borderRadius: 25,
                                        textAlign: "center",
                                        fontSize: "18pt",
                                        paddingTop: 5,
                                    }}
                                >
                                    {messageUser.slice(0, 2).toUpperCase()}
                                </div>
                            )}
                            <div
                                style={{
                                    background: user === messageUser ? "#58bf56" : "#e5e6ea",
                                    color: user === messageUser ? "white" : "black",
                                    padding: "1em",
                                    borderRadius: "1em",
                                    maxWidth: "60%"
                                }}
                            >
                                {content}
                            </div>
                        </div>
                    ) : null
            )}
        </>
    )
}

const Chat = () => {
    let user = useSelector(state => state.starting.starting.name)
    let room = useSelector(state => state.starting.starting.chatRoom)
    const [state, stateSet] = useState({
        user: user.value,
        content: "",
        room: room.value
    })


    const [postMessage] = useMutation(POST_MESSAGES)
    const onSend = () => {
        if (state.content.length > 0) {
            postMessage({
                variables: state
            })
        }
        stateSet({
            ...state,
            content: '',
        })
    }
    return (
        <div className="container">
            <Messages user={state.user} room={state.room} />
            <div className="row">
                <div className="col-8" style={{ padding: 0 }}>
                    <div className="input-group mb-2">
                        <input type="text" className="form-control" placeholder="Type here .." aria-label="Type here .." aria-describedby="button-addon2"
                            value={state.content}
                            onChange={(event) => stateSet({
                                ...state,
                                content: event.target.value
                            })}
                            onKeyUp={(event) => {
                                if (event.keyCode === 13) {
                                    onSend()
                                }
                            }}
                        />
                        <div className="col-2 ms-2">
                            <button className="btn btn btn-primary" type="button" id="button-addon2" onClick={() => onSend()}>Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default () => (
    <ApolloProvider client={client}>
        <Chat />
    </ApolloProvider>
)