import React, { Component } from 'react';
import Chat from './Chat';

import { connect } from 'react-redux'

class Home extends Component {

    render() {
        this.props.starting.users.push(this.props.starting.name.value)

        return (
            <div className="container card mt-4" >
                <div className="row">
                    <div className="col-4 card">
                        <div key={this.props.starting.chatRoom.value}>
                            <p>Room Name :</p>
                            <p>{this.props.starting.chatRoom.value}</p>
                        </div>
                        <div key={this.props.starting.name.value}>
                            <p>Users:</p>
                            {this.props.starting.users.map(element => 
                                <p>{element}</p>
                            )}
                        </div>
                    </div>

                    <div className="col-8 card">
                        <Chat /> </div>

                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    starting: state.starting.starting
})


export default connect(mapStateToProps)(Home);