const { GraphQLServer, PubSub } = require("graphql-yoga")

const messages = []

const typeDefs = `
type Message {
    id: ID!
    user: String!
    content: String!
    chatRoom:String!
}

type Query {
    messages: [Message!]
}

type Mutation {
    postMessage(user: String!, content : String! , chatRoom:String!): ID!
}
type Subscription{
    messages:[Message!]
}
`
const subscribers = []
const onMessagesUpdates = (fn) => subscribers.push(fn)

const resolvers = {
    Query: {
        messages: () => messages,

    },
    Mutation: {
        postMessage: (parent, { user, content, chatRoom }) => {
            const id = messages.length;
            messages.push({
                id,
                user,
                content,
                chatRoom
            })
            subscribers.forEach(fn => fn())
            return id;
        }
    },
    Subscription: {
        messages: {
            subscribe: (parent, args, { PubSub }) => {
                const channel = Math.random().toString(36).slice(2, 15)
                onMessagesUpdates(() => pubSub.publish(channel, { messages }))
                setTimeout(() => pubSub.publish(channel, { messages }), 0)
                return pubSub.asyncIterator(channel)
            }
        }
    }
}

const pubSub = new PubSub()
const server = new GraphQLServer({ typeDefs, resolvers, context: { pubSub } });

server.start(({ port }) => {
    console.log(`Server on http://localhost:${port}/`)
})